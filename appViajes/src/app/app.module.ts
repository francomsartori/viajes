import { BrowserModule } from '@angular/platform-browser';
import { APP_INITIALIZER, Injectable, InjectionToken, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StoreModule as NgRxStoreModule, ActionReducerMap, Store } from '@ngrx/store';
import { EffectsModule }  from '@ngrx/effects';
import { HttpClient, HttpClientModule, HttpHandler, HttpHeaders, HttpRequest } from '@angular/common/http'; 
import  Dexie  from 'dexie';
import { NgxMapboxGLModule } from 'ngx-mapbox-gl';;
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DestinoViajeComponent } from './components/destino-viaje/destino-viaje.component';
import { ListaDestinosComponent } from './components/lista-destinos/lista-destinos.component';
import { DestinoDetalleComponent } from './components/destino-detalle/destino-detalle.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormDestinoViajeComponent } from './components/form-destino-viaje/form-destino-viaje.component';
import { DestinosViajesEffects, DestinosViajesState, InitMyDataAction, intializeDestinosViajesState, reducerDestinosViajes } from './models/destinos-viaje-state.model';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { LoginComponent } from './components/login/login/login.component';
import { ProtectedComponent } from './components/protected/protected/protected.component';
import { DestinosApiClient } from './models/destinos-api-client.model';
import { AuthService } from './services/auth.service';
import { UsuarioLogueadoGuard } from './guards/usuario-logueado.guard';
import { VuelosMainComponentComponent } from './components/vuelos/vuelos-main-component/vuelos-main-component.component';
import { VuelosComponentComponent } from './components/vuelos/vuelos-component/vuelos-component.component';
import { VuelosMasInfoComponentComponent } from './components/vuelos/vuelos-mas-info-component/vuelos-mas-info-component.component';
import { VuelosDetalleComponentComponent } from './components/vuelos/vuelos-detalle-component/vuelos-detalle-component.component';
import { ReservasModule } from './reservas/reservas.module';
import { from, fromEventPattern, Observable } from 'rxjs';
import { DestinoViaje } from './models/destinos-viaje.model';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { flatMap } from 'rxjs/operators';
import { EspiameDirective } from './espiame.directive';
import { TrackerClickDirective } from './tracker-click.directive';

// config
export interface AppConfig{
  apiEndpoint : string;
}

const APP_CONFIG_VALUE: AppConfig = {
  apiEndpoint : 'http://localhost:3000'
}

export const APP_CONFIG = new InjectionToken<AppConfig>('app.config');
// -config

// routing

export const childrenRoutesVuelos: Routes = [
  { path: '', redirectTo: 'main', pathMatch : 'full'},
  { path: 'main', component: VuelosMainComponentComponent},
  { path: 'mas-info', component: VuelosMasInfoComponentComponent},
  { path: ':id', component: VuelosDetalleComponentComponent}
];

const routes : Routes =[
  { path : '', redirectTo : 'home', pathMatch: 'full'},
  { path : 'home', component : ListaDestinosComponent},
  { path : 'destino', component : DestinoDetalleComponent},
  { path : 'login', component : LoginComponent},
  {
    path: 'protected',
    component: ProtectedComponent,
    canActivate: [UsuarioLogueadoGuard]
  },
  {
    path: 'vuelos',
    component: VuelosComponentComponent,
    canActivate: [UsuarioLogueadoGuard],
    children: childrenRoutesVuelos
  }
]

// -routing

//redux
export interface AppState {
  destinos: DestinosViajesState;
};

const reducers: ActionReducerMap<AppState> = {
  destinos: reducerDestinosViajes
};

let reducersInitialState = {
    destinos: intializeDestinosViajesState()
};
// -redux

// init

export function init_app(AppLoadService: AppLoadService): () => Promise<any> {
  return () => AppLoadService.initializeDestinosViajesState();
}

@Injectable()
class AppLoadService{
  constructor(private store: Store<AppState>, private http: HttpClient){}

  async initializeDestinosViajesState(): Promise<any> {
    const headers : HttpHeaders = new HttpHeaders({'X-API-TOKEN' : 'token-seguridad'});
    const req = new HttpRequest('GET', APP_CONFIG_VALUE.apiEndpoint + '/my', {headers : headers});
    const response : any = await this.http.request(req).toPromise();
    this.store.dispatch(new InitMyDataAction(response.body));
  }
}

// -init

// dexie

export class Translation{
  constructor(public id: number, public lang: string, public key: string, public value: string) {};
}

@Injectable({
  providedIn: 'root'
})
export class MyDatabase extends Dexie {
  destinos: Dexie.Table<DestinoViaje, number>;
  translations: Dexie.Table<Translation, number>;

  constructor(){
    super('MyDatabase');
    this.version(1).stores({
      destinos: '++id, nombre, imagenUrl',
    });
    this.version(2).stores({
      destinos: '++id, nombre, imagenUrl',
      translations: '++id, lang, key, value'
    });
  };
};

export const db = new MyDatabase();

// -dexie

// i18n

class TranslationLoader implements TranslateLoader {
  constructor(private http: HttpClient){}
  
  getTranslation(lang: string): Observable<any>{
    const promise = db.translations.where('lang').equals(lang).toArray().then(results => {
      if (results.length === 0){
        return this.http.get<Translation[]>(APP_CONFIG_VALUE.apiEndpoint + '/api/translation?lang=' + lang).toPromise().then(apiResults => {
          db.translations.bulkAdd(apiResults);
          return apiResults;
        })
      };
      return results;
    }).then((traducciones) => {
      console.log('traducciones cargadas');
      console.log(traducciones);
      return traducciones;
    }).then((traducciones) => {
      return traducciones.map((t) => ({[t.key]: t.value }));
    });
    return from(promise).pipe(flatMap((elems) => from(elems)));
  };
};

function HttpLoaderFactory(http: HttpClient){
  return new TranslationLoader(http);
}

// -i18n


@NgModule({
  declarations: [
    AppComponent,
    DestinoViajeComponent,
    ListaDestinosComponent,
    DestinoDetalleComponent,
    FormDestinoViajeComponent,
    LoginComponent,
    ProtectedComponent,
    VuelosMainComponentComponent,
    VuelosComponentComponent,
    VuelosMasInfoComponentComponent,
    VuelosDetalleComponentComponent,
    EspiameDirective,
    TrackerClickDirective   
  ],
  imports: [
    BrowserModule,
    FormsModule,              
    ReactiveFormsModule,   
    HttpClientModule,   
    RouterModule.forRoot(routes), 
    NgRxStoreModule.forRoot(reducers, { initialState: reducersInitialState,
    runtimeChecks:{
      strictStateImmutability: false,
      strictActionImmutability: false,
    } 
    }),//redux
    EffectsModule.forRoot([DestinosViajesEffects]),
    StoreDevtoolsModule.instrument(),
    HttpClientModule,
    ReservasModule ,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (HttpLoaderFactory),
        deps: [HttpClient]
      }
    }),
    NgxMapboxGLModule,
    BrowserAnimationsModule
  ],
  providers: [ AuthService, UsuarioLogueadoGuard,
  {provide: APP_CONFIG, useValue: APP_CONFIG_VALUE},
  AppLoadService,
  {provide: APP_INITIALIZER, useFactory: init_app, deps: [AppLoadService], multi: true},
  MyDatabase],
  bootstrap: [AppComponent]
})
export class AppModule { }
