import {
    reducerDestinosViajes,
    DestinosViajesState,
    intializeDestinosViajesState,
    InitMyDataAction,
    NuevoDestinoAction
} from './destinos-viaje-state.model';

import { DestinoViaje } from './destinos-viaje.model';

describe('reducerDestinosViajes', () => {
    it('should reduce init data', () => {
        // setup
        const prevState: DestinosViajesState = intializeDestinosViajesState();
        const action: InitMyDataAction = new InitMyDataAction(['destino 1', 'destino 2']);
        // action
        const newState: DestinosViajesState = reducerDestinosViajes(prevState, action);
        // assert
        expect(newState.items.length).toEqual(2);
        expect(newState.items[0].nombre).toEqual('destino 1');
    });

    it('should reduce new item data', () => {
        // setup
        const prevState: DestinosViajesState = intializeDestinosViajesState();
        const action: NuevoDestinoAction = new NuevoDestinoAction(new DestinoViaje('barcelona', 'url'));
        // action
        const newState: DestinosViajesState = reducerDestinosViajes(prevState, action);
        // assert
        expect(newState.items.length).toEqual(1);
        expect(newState.items[0].nombre).toEqual('barcelona');
    });  
});


