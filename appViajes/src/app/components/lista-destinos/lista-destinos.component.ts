import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { DestinoViaje } from '../../models/destinos-viaje.model';
import { DestinosApiClient } from '../../models/destinos-api-client.model';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.module';
import { ElegidoFavoritoAction, NuevoDestinoAction } from '../../models/destinos-viaje-state.model';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css'],
  providers: [ DestinosApiClient ]
})
export class ListaDestinosComponent implements OnInit {

  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  updates: string[];
  all;

  constructor(public DestinosApiClient : DestinosApiClient, private store: Store<AppState>) { 
    this.onItemAdded = new EventEmitter();
    this.updates = [];

    this.store.select(state => state.destinos.favorito)
      .subscribe(data => {
        const d = data;
        if (d != null) {
          this.updates.push('Se ha elegido a : ' + d.nombre);
        }
      });

      store.select(state => state.destinos.items).subscribe(items => this.all = items);
   }

   ngOnInit(): void {
  }

  agregado(d : DestinoViaje){
    this.DestinosApiClient.add(d);
    this.onItemAdded.emit(d);
  //  this.store.dispatch(new NuevoDestinoAction(d));
  }

  elegido(e : DestinoViaje){
    this.DestinosApiClient.elegir(e);
 //   this.store.dispatch(new ElegidoFavoritoAction(e));
  }

  getAll(){

  }
}
