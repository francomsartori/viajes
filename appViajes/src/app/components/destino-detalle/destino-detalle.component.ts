import { Component, Inject, inject, InjectionToken, OnInit } from '@angular/core';
import { DestinosApiClient } from './../../models/destinos-api-client.model';
import { DestinoViaje } from './../../models/destinos-viaje.model';
import { ActivatedRoute } from '@angular/router';
import { AppState } from 'src/app/app.module';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-destino-detalle',
  templateUrl: './destino-detalle.component.html',
  styleUrls: ['./destino-detalle.component.css'],
  providers: [DestinosApiClient]
  })
  
  export class DestinoDetalleComponent implements OnInit {
    destino : DestinoViaje;

    style={
      sources: {
        world: {
          type: 'geojson',
          data: 'https://raw.githubusercontent.com/johan/world.geo.json/master/countries.geo.json'
        }
      },
      version: 8,
      layers: [{
        'id' : 'countries',
        'type' : 'fill',
        'source' : 'world',
        'layout' : {},
        'paint': { 'fill-color' : '#6F788A'}
      }]
    };

    constructor(private route: ActivatedRoute, private destinosApiClient: DestinosApiClient) { }

    ngOnInit(): void {
      const id = this.route.snapshot.paramMap.get('id');
      this.destino = this.destinosApiClient.getById(id);
    }
  }


// SE COMENTA ESTA PARTE PORQUE LOS VIDEOS DEL CURSO ESTAN INCOMPLETOS
/*
class DestinosApiClientViejo{
  getById(id: string): DestinoViaje {
    console.log('llamado por la clase vieja');
    return null;
  }
}

class DestinosApiClientDecorated extends DestinosApiClient {
  constructor(@Inject(APP_CONFIG) private config: AppConfig, store: Store<AppState>){
    super(store);
  }

  getById(id: string): DestinoViaje{
    console.log('llamado por la decorada');
    console.log('config: ' + this.config.apiEndpoint);
    return super.getById(id);
  }
}

@Component({
  selector: 'app-destino-detalle',
  templateUrl: './destino-detalle.component.html',
  styleUrls: ['./destino-detalle.component.css'],
  providers: [
    {provide: APP_CONFIG, useValue: APP_CONFIG_VALUE},
    {provide: DestinosApiClient, useClass: DestinosApiClientDecorated},
    {provide: DestinosApiClientViejo, useExisting: DestinosApiClient}
  ]
})

export class DestinoDetalleComponent implements OnInit {
  destino : DestinoViaje;

  constructor(private route: ActivatedRoute, private destinosApiClient: DestinosApiClientViejo) { }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.destino = this.destinosApiClient.getById(id);
  }

  
}
*/